pub fn price_from_ints(dollars: i32, cents: i32) -> f32 {
    //string add to avoid FPE
    format!("{}.{:02}", dollars.to_string(), cents.to_string())
        .trim()
        .parse()
        .unwrap()
}
