use crate::database::DatabaseError;
use crate::models::Image;
use diesel::pg::PgConnection;
use diesel::prelude::*;

pub fn get_images_for(find_for_id: i32, connection: &PgConnection) -> Result<Vec<Image>, DatabaseError> {
    use crate::schema::images::dsl::*;

    Ok(images
        .filter(product_id.eq(find_for_id))
        .load::<Image>(connection)?)
}
