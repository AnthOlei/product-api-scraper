use super::errors::ScraperError;
use super::parse::{begin_scraping, insert_or_update_product, generate_product_without_insert};
use crate::database::PgPooledConnection;
use crate::database::IntermediatePersistanceLayer;
use crate::models::Product;
use std::sync::Arc;

//scraper todo
//add multiple products, sweep through cleanup
//multiple catagories
pub async fn run_scraper(url: String, commit: bool, kind: Option<String>, conn: PgPooledConnection, ipl: Arc<IntermediatePersistanceLayer>) -> Result<Product, ScraperError> {
    //begin scraping
    let scraper = begin_scraping(&url, kind).await?;

    if commit {
        insert_or_update_product(scraper, ipl, &conn)
    } else {
        let product = generate_product_without_insert(scraper);
        Ok(product)
    }
}
