use crate::database::{IntermediatePersistanceLayer, PgPooledConnection};
use crate::models::Product;
use crate::diesel::prelude::*;
use super::product_scrapers::product_scraper::ProductScraper;
use super::product_scrapers::{GuaranteedPart, OptionalPart};

//WARNING!!!!!! ONLY works for two point precision.
//works for prices (12.20 -> (12, 20)) but anything after gets truncated, (12.201 -> (12, 20))
pub fn split_float_into_ints(float: f32) -> (i32, i32) {
    let dollars = float.floor() as i32;
    let cents = ((float * 100.0) as i32) - dollars * 100;
    (dollars, cents as i32)
}

pub fn get_optional_price_tuple(maybe_price: Option<f32>) -> (Option<i32>, Option<i32>) {
    if let Some(sale_price) = maybe_price {
        let sale_tuple = split_float_into_ints(sale_price);
        (Some(sale_tuple.0), Some(sale_tuple.1))
    } else {
        (None, None)
    }
}

pub fn check_exact_product_exists<'a>(
    scraper: &Box<dyn ProductScraper + 'a>,
    persistance_layer: &IntermediatePersistanceLayer,
    connection: &PgPooledConnection,
) -> Option<Product> {
    use crate::schema::products::dsl::*;

    let product_query = products
        .filter(title.eq(scraper.get_guaranteed_part(GuaranteedPart::Title)))
        .load::<Product>(connection)
        .unwrap();

    if !product_query.is_empty() {
        //loops through each product, stopping if we found the same color.
        //otherwise, we don't
        for model in product_query {
            let maybe_color_id = scraper
                .get_optional_part(OptionalPart::Color)
                .and_then(|v| persistance_layer.get_color_id(v));
            if model.color_id == maybe_color_id {
                return Some(model);
            }
        }
    }
    None //no product with that title in the database, or the loop never hit an exact match
}