pub mod driver;
pub use driver::run_scraper;

mod parse;

mod utils;

mod product_scrapers;

pub mod errors;
pub use errors::ScraperError;

