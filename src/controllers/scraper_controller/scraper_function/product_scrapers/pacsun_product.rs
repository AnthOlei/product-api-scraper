use super::product_scraper::{GuaranteedPart, OptionalPart, ProductScraper};
use super::{select_inner_html_by_element_type, get_base_url};
use crate::controllers::scraper_controller::errors::ScraperError;
use scraper::{Html, Selector};
use std::collections::HashMap;

use serde::{Serialize, Deserialize};
use serde_json;

#[derive(Serialize, Deserialize, Debug)]
struct PacsunProductJson {
  color: String,
  description: String,
  image: Vec<String>,
}

pub struct PacsunProductScraper<'a> {
    url: &'a str,
    body: Html,
    json: PacsunProductJson,
    kind: Option<String>
}

impl PacsunProductScraper<'_> {
    pub fn new(url: &str, body: Html, kind: Option<String>) -> Result<PacsunProductScraper, ScraperError> {
        let selector = Selector::parse("script").unwrap();
        let all_scripts = body.select(&selector);
        let mut json: Option<PacsunProductJson> = None;
        //grabs the json in the header that page was populated with
        for element in all_scripts {
            if let Some("application/ld+json") = element.value().attr("type") {
                let parsed_json = &element.inner_html().replace("\n", "").replace("\"", r#"""#).replace("\t", "");
                json = serde_json::from_str(parsed_json).unwrap();
                break;
            }
        }

        if json.is_none(){
            return Err(ScraperError::NoValidDataError);
        }

        Ok(PacsunProductScraper {
            url: get_base_url(url),
            body,
            json: json.unwrap(),
            kind
        })
    }
}

impl PacsunProductScraper<'_> {
    fn get_color(&self) -> Option<String> {
        let val = select_inner_html_by_element_type(&self.body, "div.rwd-swatch-value");
        let value = val.first().unwrap().to_owned();
        if value.contains(':') {
            Some(value.split(':').nth(1).unwrap().trim().to_string())
        } else {
            Some(value)
        }
    }

    fn get_desc(&self) -> Option<String> {
        let selector = Selector::parse("div.rwd-pdp-desc-container").unwrap();
        let p_selector = Selector::parse("p").unwrap();
        Some(self.body
            .select(&selector)
            .last()
            .unwrap()
            .select(&p_selector)
            .nth(0)
            .unwrap()
            .inner_html())
    }

    fn get_brand(&self) -> Option<String> {
        let selector = Selector::parse("h1").unwrap();
        Some(self.body
            .select(&selector)
            .last()
            .unwrap()
            .value()
            .attr("data-brand")
            .unwrap()
            .to_owned())
    }

    fn get_title(&self) -> String {
        let val = select_inner_html_by_element_type(&self.body, "h1");
        val.first().unwrap().to_owned()
    }
}

impl ProductScraper for PacsunProductScraper<'_> {
    fn get_optional_part(&self, part: OptionalPart) -> Option<String> {
        match part {
            OptionalPart::Color => self.get_color(),
            OptionalPart::Description => self.get_desc(),
            OptionalPart::Brand => self.get_brand(),
        }
    }

    fn get_guaranteed_part(&self, part: GuaranteedPart) -> String {
        match part {
            GuaranteedPart::Title => self.get_title(),
            GuaranteedPart::Url => self.url.to_owned(),
            GuaranteedPart::Seller => "Pacsun".to_owned(),
        }
    }

    fn get_kind(&self, catagories: HashMap<String, i32>) -> Option<i32> {
        if let Some(kind) = &self.kind {
            return Some(
                catagories
                    .get(kind)
                    .unwrap_or_else(|| panic!("No catagory found with name {}", kind))
                    .to_owned(),
            );
        }
        for word in self.get_guaranteed_part(GuaranteedPart::Title).split(' ') {
            let maybe_catagory = catagories.get(word);
            match maybe_catagory {
                None => continue,
                Some(_) => return maybe_catagory.map(|v| v.to_owned()),
            };
        }
        None
    }

    fn get_price(&self) -> f32 {
        let val = select_inner_html_by_element_type(&self.body, "div.usd-price");
        val.first().unwrap().to_owned().parse().unwrap()
    }

    fn get_sale_price(&self) -> Option<f32> {
        let val = select_inner_html_by_element_type(&self.body, "div.price-promo");
        match val.first() {
            None => None,
            //gets rid of the "New price: $ before the price"
            Some(_) => val
                .first()
                .and_then(|e| {
                    e.split('$')
                        .nth(1)
                        .map(|s| s.trim())
                        .map(|s| s.parse::<f32>())
                })
                .and_then(|r| r.ok()),
        }
    }

    fn get_images(&self) -> Vec<String> {
        self.json.image.to_owned()
    }
}
