use std::collections::HashMap;

pub enum GuaranteedPart {
    Title,
    Url,
    Seller,
}

pub enum OptionalPart {
    Color,
    Description,
    Brand,
}


pub trait ProductScraper{
    fn get_optional_part(&self, part: OptionalPart) -> Option<String>;
    fn get_guaranteed_part(&self, part: GuaranteedPart) -> String;
    fn get_sale_price(&self) -> Option<f32>;
    fn get_price(&self) -> f32;
    //takes hashmap of Kind, fk, and returns a FK.
    fn get_kind(&self, catagories: HashMap<String, i32>) -> Option<i32>;
    fn get_images(&self) -> Vec<String>;
}