use super::get_base_url;
use super::product_scraper::{GuaranteedPart, OptionalPart, ProductScraper};
use std::collections::HashMap;

use serde::{Deserialize, Serialize};
use serde_json;
use serde_json::Value;

#[derive(Serialize, Deserialize, Debug)]
struct AsosBrand {
    name: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct AsosVariant {
    colour: String,
    price: AsosPrice,
}

#[derive(Serialize, Deserialize, Debug)]
struct AsosPrice {
    current: AsosPriceValue,
    previous: AsosPriceValue,
    currency: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct AsosPriceValue {
    value: f32,
}

#[derive(Serialize, Deserialize, Debug)]
struct AsosMedia {
    images: Vec<AsosImage>,
}

#[derive(Serialize, Deserialize, Debug)]
struct AsosImage {
    url: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct AsosProductJson {
    id: i64,
    name: String,
    description: String,
    brand: AsosBrand,
    variants: Vec<AsosVariant>,
    media: AsosMedia,
}

#[derive(Serialize, Deserialize, Debug)]
struct IdJson {
    id: i64,
}

pub struct AsosProductScraper<'a> {
    url: &'a str,
    json: AsosProductJson,
    kind: Option<String>,
}

impl AsosProductScraper<'_> {
    pub async fn new(
        url: &str,
        body_string: String,
        kind: Option<String>,
    ) -> AsosProductScraper<'_> {
        let parsed_json = body_string
            .split("window.asos.pdp.config.product = ")
            .collect::<Vec<&str>>()
            .to_owned()[1]
            .split(';')
            .next()
            .to_owned()
            .unwrap()
            .replace("\n", "")
            .replace("\"", r#"""#)
            .replace("\t", ""); //TODO take the josn that is in the log and use it so we don't spam the APi
        let val = serde_json::from_str::<Value>(&parsed_json).unwrap();
        AsosProductScraper {
            url: get_base_url(url),
            json: make_asos_api_request(val["id"].to_string()).await,
            kind,
        }
    }
}

impl AsosProductScraper<'_> {
    fn get_color(&self) -> Option<String> {
        Some(
            self.json
                .variants
                .iter()
                .find(|variant| variant.price.currency == "USD")
                .unwrap()
                .colour
                .clone(),
        )
    }
}

impl ProductScraper for AsosProductScraper<'_> {
    fn get_optional_part(&self, part: OptionalPart) -> Option<String> {
        match part {
            OptionalPart::Color => self.get_color(),
            OptionalPart::Description => Some(self.json.description.clone()),
            OptionalPart::Brand => Some(self.json.brand.name.clone()),
        }
    }

    fn get_guaranteed_part(&self, part: GuaranteedPart) -> String {
        match part {
            GuaranteedPart::Title => self.json.name.clone(),
            GuaranteedPart::Url => self.url.to_owned(),
            GuaranteedPart::Seller => "Asos".to_owned(),
        }
    }

    //if kind is passed, we try to use that. If not, we try to infer it based off of the title.
    //otherwise, we just return none.
    fn get_kind(&self, catagories: HashMap<String, i32>) -> Option<i32> {
        if let Some(kind) = &self.kind {
            return Some(
                catagories
                    .get(kind)
                    .unwrap_or_else(|| panic!("No catagory found with name {}", kind))
                    .to_owned(),
            );
        }
        for word in self.get_guaranteed_part(GuaranteedPart::Title).split(' ') {
            let maybe_catagory = catagories.get(word);
            match maybe_catagory {
                None => continue,
                Some(_) => return maybe_catagory.map(|v| v.to_owned()),
            };
        }
        None
    }

    fn get_price(&self) -> f32 {
        let current = self
            .json
            .variants
            .iter()
            .find(|variant| variant.price.currency == "USD")
            .unwrap()
            .price
            .current
            .value;

        let prev = self
            .json
            .variants
            .iter()
            .find(|variant| variant.price.currency == "USD")
            .unwrap()
            .price
            .previous
            .value;

        //return greater of 2
        if current > prev {
            current
        } else {
            prev
        }
    }

    fn get_sale_price(&self) -> Option<f32> {
        let current = self
            .json
            .variants
            .iter()
            .find(|variant| variant.price.currency == "USD")
            .unwrap()
            .price
            .current
            .value;

        let prev = self
            .json
            .variants
            .iter()
            .find(|variant| variant.price.currency == "USD")
            .unwrap()
            .price
            .previous
            .value;

        if current < prev {
            Some(current)
        } else {
            None
        }
    }

    fn get_images(&self) -> Vec<String> {
        self.json
            .media
            .images
            .iter()
            .map(|image| image.url.clone())
            .map(|image| format!("http://{}", image))
            .collect()
    }
}

async fn make_asos_api_request(product_id: String) -> AsosProductJson {
    let client = reqwest::Client::new();
    let response = &client
        .get("https://asos2.p.rapidapi.com/products/v3/detail")
        .query(&[
            ("store", "us"),
            ("sizeSchema", "US"),
            ("lang", "en-US"),
            ("currency", "USD"),
            ("id", &product_id),
        ])
        .header("x-rapidapi-host", "asos2.p.rapidapi.com")
        .header(
            "x-rapidapi-key",
            "444c598cddmshe4d5cadb55cfab6p179e79jsne283674d2790",
        )
        .header("useQueryString", "true")
        .send()
        .await
        .unwrap()
        .text()
        .await
        .unwrap();
    serde_json::from_str::<AsosProductJson>(response).unwrap()
}
