use scraper::{Html, Selector};

//given a body and element type, return a vector of inner html.
pub fn select_inner_html_by_element_type<'a>(body: &'a Html, element: &str) -> Vec<String> {
    let selector = Selector::parse(element).unwrap();
    let mut elements: Vec<String> = Vec::new();
    for element in body.select(&selector) {
        elements.push(element.inner_html());
    }
    elements
}

pub fn get_base_url(url: &str) -> &str {
    url.split('?').collect::<Vec<&str>>()[0]
}

