use super::errors::ScraperError;
use super::product_scrapers::product_scraper::ProductScraper;
use super::product_scrapers::{AsosProductScraper, PacsunProductScraper};
use super::product_scrapers::{GuaranteedPart, OptionalPart};
use super::utils::{check_exact_product_exists, get_optional_price_tuple, split_float_into_ints};
use crate::database::{IntermediatePersistanceLayer, PgPooledConnection};
use crate::database::{ImageInsertable, ProductInsertable};
use crate::diesel::prelude::*;
use crate::diesel::RunQueryDsl;
use crate::models::Product;
use crate::schema::{images, products};
use crate::scraper::html::Html;
use std::sync::Arc;
use reqwest::header::USER_AGENT;

enum Source {
    Pacsun,
    Asos,
}

//takes a string and returns the product scraper with the get element methods
#[allow(clippy::needless_lifetimes)]
pub async fn begin_scraping<'a>(
    source: &'a str, kind: Option<String>,
) -> Result<Box<dyn ProductScraper + 'a>, ScraperError> {
    let scraper: Box::<dyn ProductScraper> = match match_source(source)? {
        Source::Pacsun => Box::new(PacsunProductScraper::new(
            &source,
            Html::parse_document(&reqwest::get(source).await?.text().await?), kind)?),
        Source::Asos => Box::new(AsosProductScraper::new(
            &source,
            (&reqwest::Client::new()
            .get(source)
            .header(USER_AGENT,"Mozilla/5.0 (Macintosh; Intel Mac OS iX 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36")
            .send()
            .await?
            .text().await?).to_owned(), kind).await),
    };

    Ok(scraper)
}

pub fn insert_or_update_product<'a>(
    scraper: Box<dyn ProductScraper + 'a>,
    intermidiate_persistance_layer: Arc<IntermediatePersistanceLayer>,
    connection: &PgPooledConnection,
) -> Result<Product, ScraperError> {
    let product_result;
    if let Some(model) =
        check_exact_product_exists(&scraper, &intermidiate_persistance_layer, connection)
    {
        println!("product already in databse, running update");
        product_result = update_product(scraper, connection, model);
    } else {
        println!("product not in database, running insert");
        product_result = insert_product(scraper, intermidiate_persistance_layer, connection);
    }
    product_result
}

fn update_product<'a>(
    scraper: Box<dyn ProductScraper + 'a>,
    connection: &PgPooledConnection,
    queried_product: Product,
) -> Result<Product, ScraperError> {
    use crate::schema::products::dsl::*;
    if check_product_needs_update(&scraper, &queried_product) {
        let maybe_sale_tuple = get_optional_price_tuple(scraper.get_sale_price());
        let product = diesel::update(products.find(queried_product.id))
            .set((
                price_dollar.eq(split_float_into_ints(scraper.get_price()).0),
                price_cents.eq(split_float_into_ints(scraper.get_price()).1),
                sale_dollar.eq(maybe_sale_tuple.0),
                sale_cents.eq(maybe_sale_tuple.1),
            ))
            .get_result::<Product>(connection)
            .expect("attempted update, but couldn't find id");
        debug!("updated price");
        Ok(product) //returns new product
    } else {
        Ok(queried_product) //no update was needed, return old product
    }
}

fn insert_product<'a>(
    scraper: Box<dyn ProductScraper + 'a>,
    intermidiate_persistance_layer: Arc<IntermediatePersistanceLayer>,
    connection: &PgPooledConnection,
) -> Result<Product, ScraperError> {
    let maybe_sale_tuple = get_optional_price_tuple(scraper.get_sale_price());
    let insertable = ProductInsertable {
        brand_id: scraper.get_optional_part(OptionalPart::Brand).map(|v| {
            intermidiate_persistance_layer
                .get_or_insert_brand(v, &connection)
                .unwrap()
        }),
        color_id: scraper.get_optional_part(OptionalPart::Color).map(|v| {
            intermidiate_persistance_layer
                .get_or_insert_color(v, &connection)
                .unwrap()
        }),
        kind_id: scraper.get_kind(intermidiate_persistance_layer.get_kinds()),
        details: scraper.get_optional_part(OptionalPart::Description),
        source: scraper.get_guaranteed_part(GuaranteedPart::Url),
        title: scraper.get_guaranteed_part(GuaranteedPart::Title),
        price_dollar: split_float_into_ints(scraper.get_price()).0,
        price_cents: split_float_into_ints(scraper.get_price()).1,
        sale_dollar: maybe_sale_tuple.0,
        sale_cents: maybe_sale_tuple.1,
        seller: scraper.get_guaranteed_part(GuaranteedPart::Seller),
    };

    let product = diesel::insert_into(products::table)
        .values(insertable)
        .get_result::<Product>(connection)
        .unwrap();

    let mut images: Vec<ImageInsertable> = Vec::new();
    for source in scraper.get_images() {
        images.push(ImageInsertable {
            product_id: product.id,
            source,
        });
    }
    if !images.is_empty() {
        diesel::insert_into(images::table)
            .values(images)
            .execute(connection)
            .expect("failed inserting images into the database");
    }

    Ok(product)
}

pub fn generate_product_without_insert<'a>(scraper:  Box<dyn ProductScraper + 'a>) -> Product {
    let maybe_sale_tuple = get_optional_price_tuple(scraper.get_sale_price());
    Product {
            id: 0,
            brand_id: None,
            color_id: None,
            kind_id: None,
            details: scraper.get_optional_part(OptionalPart::Description),
            source: scraper.get_guaranteed_part(GuaranteedPart::Url),
            title: scraper.get_guaranteed_part(GuaranteedPart::Title),
            price_dollar: split_float_into_ints(scraper.get_price()).0,
            price_cents: split_float_into_ints(scraper.get_price()).1,
            sale_dollar: maybe_sale_tuple.0,
            sale_cents: maybe_sale_tuple.1,
            seller: scraper.get_guaranteed_part(GuaranteedPart::Seller),
        }
}

//helper to compare if prices (sale, real) of a scrape and query (model)
//need updating. will return TRUE if scraped sale/real price != query sale/real price
fn check_product_needs_update<'a>(
    //TODO THIS FN
    scraper: &Box<dyn ProductScraper + 'a>,
    queried_product: &Product,
) -> bool {
    let sale_price = queried_product.get_sale_price();

    let sale_match = match (scraper.get_sale_price(), sale_price) {
        (None, None) => false,
        (Some(_), None) => false,
        (None, Some(_)) => false,
        (Some(o), Some(v)) => (o - v).abs() < std::f32::EPSILON,
    };
    let price_match = scraper.get_price() - queried_product.get_price() < std::f32::EPSILON;
    !(sale_match && price_match)
}

fn match_source(url: &str) -> Result<Source, ScraperError> {
    if url.contains("pacsun") {
        return Ok(Source::Pacsun);
    } else if url.contains("asos") {
        return Ok(Source::Asos);
    }

    Err(ScraperError::InvalidSourceError)
}
