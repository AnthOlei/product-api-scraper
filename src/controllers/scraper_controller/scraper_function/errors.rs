use std::fmt;
use reqwest::Error as reqwestStdError;
use failure::Fail;

#[derive(Debug, Fail)]
pub enum ScraperError {
    InvalidSourceError,
    NoValidDataError,
    ReqwestError(reqwestStdError),
}

impl fmt::Display for ScraperError {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &*self {
            ScraperError::InvalidSourceError => f.write_str("Invalid URL for scraping."),
            ScraperError::NoValidDataError => write!(f, "Couldn't find required data.. did they update their website / API?"),
            ScraperError::ReqwestError(ref e) => e.fmt(f),
        }
    }
}

impl std::convert::From<reqwest::Error> for ScraperError {
    fn from(error: reqwestStdError) -> Self {
        ScraperError::ReqwestError(error)
    }
}