use super::inputs::ScrapeUrl;
use super::scraper_function::run_scraper;
use crate::errors::ApiError;
use crate::models::Product;
use actix_web::web::Json;
use actix_web::{post, Result, web};
use crate::database::{PgPool, IntermediatePersistanceLayer};
use crate::controllers::pg_pool_handler;

#[post("/scraper/")]
pub async fn scraper_route(url_input: Json<ScrapeUrl>, pool: web::Data<PgPool>, ipl: web::Data<IntermediatePersistanceLayer>) -> Result<Json<Product>, ApiError> {
    let product = run_scraper(
        url_input.url.to_owned(),
        url_input.commit.unwrap_or(false),
        url_input.kind.as_ref().map(|kind| kind.to_owned()),
        pg_pool_handler(pool)?,
        ipl.into_inner(), //need this to be arc, as the underlying data may be modified
        ).await?;
    Ok(Json(product))
}
