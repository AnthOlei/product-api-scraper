use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct ScrapeUrl {
    pub url: String,
    pub commit: Option<bool>,
    pub kind: Option<String>
}