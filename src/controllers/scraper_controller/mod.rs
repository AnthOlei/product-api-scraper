pub mod routes;
pub use routes::*;

mod scraper_function;
pub use scraper_function::errors;

mod inputs;