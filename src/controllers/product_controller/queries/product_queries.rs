use crate::database::DatabaseError;
use crate::database::{PgPooledConnection, IntermediatePersistanceLayer};
use crate::diesel::prelude::*;
use crate::utils::get_images_for;
use crate::models::{Product, Kind, Color, Brand};
use crate::dto::{ProductQueryDto, ProductDto};

pub fn get_product_by_query(query: ProductQueryDto, connection: &PgPooledConnection, ipl: &IntermediatePersistanceLayer) -> Result<Vec<ProductDto>, DatabaseError> {
    no_arg_sql_function!(RANDOM, (), "Represents the sql RANDOM() function");


    use crate::schema::products::dsl as product_table;
    use crate::schema::brands::dsl as brand_table;
    use crate::schema::kinds::dsl as kind_table;
    use crate::schema::colors::dsl as color_table;
    
    let mut base_query = product_table::products
        .limit(query.count.into())
        .order(RANDOM)
        .filter(product_table::id.ne_all(query.seen))
        .left_outer_join(brand_table::brands)
        .left_outer_join(color_table::colors)
        .left_outer_join(kind_table::kinds)
        .into_boxed();

    if let Some(upper_bound) = query.price_upper_bound {
        base_query = base_query.filter(product_table::price_dollar.lt(upper_bound + 1));
    }

    if let Some(lower_bound) = query.price_lower_bound {
        base_query = base_query.filter(product_table::price_dollar.gt(lower_bound - 1));
    }
    if let Some(kind) = query.kind {
        base_query = base_query.filter(product_table::kind_id.eq(ipl.get_kind_id(kind)));
    }

    if let Some(brand) = query.brand {
        base_query = base_query.filter(product_table::brand_id.eq(ipl.get_brand_id(brand)));
    }

    if let Some(color) = query.color {
        base_query = base_query.filter(product_table::color_id.eq(ipl.get_color_id(color)));
    }

    let product_queries = base_query.load::<(Product, Option<Brand>, Option<Color>, Option<Kind>)>(connection)?;

    let mut product_dtos = Vec::with_capacity(product_queries.len());

    for query_result in product_queries {
        let images = get_images_for(query_result.0.id, &connection)?;
        let product_dto = ProductDto::full_from_entites(query_result.0, query_result.1, query_result.2, query_result.3, images)?;
        product_dtos.push(product_dto);
    }

    Ok(product_dtos)
    
}