use crate::database::IntermediatePersistanceLayer;

pub fn get_all_unique_brands(ipl: &IntermediatePersistanceLayer) -> Vec<String> {
    ipl.get_brands()
}

pub fn get_all_unique_colors(ipl: &IntermediatePersistanceLayer) -> Vec<String> {
    ipl.get_colors()
}
