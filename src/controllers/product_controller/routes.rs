use super::queries::get_all_unique::{get_all_unique_brands, get_all_unique_colors};
use super::queries::get_product_by_query;
use crate::controllers::pg_pool_handler;
use crate::database::{IntermediatePersistanceLayer, PgPool};
use crate::dto::ProductListDto;
use crate::dto::ProductQueryDto;
use crate::errors::ApiError;
use actix_web::web::Json;
use actix_web::{get, post, web, Result};

#[post("/products/")]
pub async fn query_products_route(
    product_query: Json<ProductQueryDto>,
    pool: web::Data<PgPool>,
    ipl: web::Data<IntermediatePersistanceLayer>,
) -> Result<Json<ProductListDto>, ApiError> {
    let products = get_product_by_query(
        product_query.into_inner(),
        &pg_pool_handler(pool)?,
        &*(ipl.into_inner()).clone(),
    )?;
    Ok(Json(ProductListDto { products }))
}

#[get("/products/brands/")]
pub async fn get_brands_route(
    ipl: web::Data<IntermediatePersistanceLayer>,
) -> Result<Json<Vec<String>>, ApiError> {
    Ok(Json(get_all_unique_brands(&*(ipl.into_inner()).clone())))
}

#[get("/products/colors/")]
pub async fn get_colors_route(
    ipl: web::Data<IntermediatePersistanceLayer>,
) -> Result<Json<Vec<String>>, ApiError> {
    Ok(Json(get_all_unique_colors(&*(ipl.into_inner()).clone())))
}
