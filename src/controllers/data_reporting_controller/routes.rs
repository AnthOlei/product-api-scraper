use actix_web::web::Json;
use actix_web::{post, Result, web, patch};
use crate::dto::{UserDataDto, FeedbackDto, StatusDto};
use crate::errors::ApiError;
use super::queries::{insert_user_data, insert_feedback, set_status};
use crate::database::PgPool;
use crate::controllers::pg_pool_handler;

#[post("/userdata/")]
pub async fn post_user_data_route(product_query: Json<UserDataDto>, pool: web::Data<PgPool>) -> Result<Json<UserDataDto>, ApiError> {
    Ok(Json(
        insert_user_data(product_query.into_inner(), pg_pool_handler(pool)?)?
    ))
}

#[post("/feedback/")]
pub async fn post_feedback_route(feedback: Json<FeedbackDto>, pool: web::Data<PgPool>) -> Result<Json<FeedbackDto>, ApiError> {
    Ok(Json(
        insert_feedback(feedback.into_inner(), &pg_pool_handler(pool)?)?
    ))
}

#[patch("/userdata/status/")]
pub async fn patch_status_route(status: Json<StatusDto>, pool: web::Data<PgPool>) -> Result<Json<StatusDto>, ApiError> {
    Ok(Json(
        set_status(status.into_inner(), pg_pool_handler(pool)?)?
    ))
}
