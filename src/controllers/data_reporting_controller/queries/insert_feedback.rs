use crate::database::{PgPooledConnection, DatabaseError, FeedbackInsertable};
use crate::diesel::prelude::*;
use crate::dto::FeedbackDto;
use crate::models::Feedback;

pub fn insert_feedback(feedback: FeedbackDto, connection: &PgPooledConnection) -> Result<FeedbackDto, DatabaseError> {
    use crate::schema::user_feedback::dsl as feedback_table;

    let insertable = FeedbackInsertable {
        email: feedback.email,
        feedback: feedback.feedback,
        user_id: feedback.user_id,
        date_created: None,
    };

    Ok(FeedbackDto::from_entity(diesel::insert_into(feedback_table::user_feedback)
        .values(&insertable)
        .get_result::<Feedback>(connection)?))
}
