use crate::database::DatabaseError;
use crate::database::PgPooledConnection;
use crate::diesel::prelude::*;
use crate::dto::StatusDto;
use crate::models::User;

pub fn set_status(
    status: StatusDto,
    connection: PgPooledConnection,
) -> Result<StatusDto, DatabaseError> {
    use crate::schema::users::dsl as user_table;

    if status.status != "Active" || status.status != "Inactive" {
        return Err(DatabaseError::InvalidStatusValue);
    }

    let updated_user = diesel::update(user_table::users.find(status.user_id))
        .set(user_table::status.eq(status.status))
        .get_result::<User>(&connection)?;

    Ok(StatusDto {
         //should never get to no value, databse defaults to active. Have no value fallback flag
         //so if we get to this case, we do get notified on the API end
        status: updated_user.status.unwrap_or_else(|| "NO VALUE".to_string()),
        user_id: updated_user.id,
    })
}
