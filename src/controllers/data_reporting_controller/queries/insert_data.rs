use crate::database::SessionInsertable;
use crate::database::{DatabaseError, PgPooledConnection};
use crate::diesel::prelude::*;
use crate::dto::UserDataDto;
use crate::models::{User, UserData};

pub fn insert_user_data(user_data: UserDataDto, connection: PgPooledConnection) -> Result<UserDataDto, DatabaseError> {
    use crate::schema::sessions::dsl as session_table;
    use crate::schema::user_data::dsl as user_data_table;
    use crate::schema::users::dsl as user_table;

    //verify the user ID is in the database
    diesel::insert_into(user_table::users)
        .values(User { id: user_data.uuid, total_minutes: user_data.total_minutes, status: Some("Active".to_owned()) })
        .on_conflict(user_table::id)
        .do_nothing()
        .execute(&connection)?;

    let queried_user_datas = user_data_table::user_data
        .filter(user_data_table::user_id.eq(user_data.uuid))
        .load::<UserData>(&connection)?;

    //add session if we have both the ending and the beginning
    if let (Some(session_start), Some(session_end)) =
        (user_data.session_start, user_data.session_end)
    {
        diesel::insert_into(session_table::sessions)
            .values(SessionInsertable {
                user_id: user_data.uuid,
                session_start: session_start.naive_utc(),
                session_end: session_end.naive_utc(),
            })
            .execute(&connection)?;
    }

    //fill in the insertable with whatever the upsert needs to insert / update
    let insertable: UserData;
    if let Some(queried_user_data) = queried_user_datas.get(0) {
        insertable = UserData {
            user_id: queried_user_data.user_id,
            dislikes: queried_user_data.dislikes.clone(),
            likes: queried_user_data.likes.clone(),
            checkout: queried_user_data.checkout.clone(),
        }
    } else {
        insertable = UserData {
            user_id: user_data.uuid,
            likes: user_data.liked_products,
            dislikes: user_data.disliked_products,
            checkout: user_data.checkout_products,
        }
    }

    //preform upsert of main "userdata"
    let upserted = diesel::insert_into(user_data_table::user_data)
        .values(&insertable)
        .on_conflict(user_data_table::user_id)
        .do_update()
        .set(&insertable)
        .get_result::<UserData>(&connection)?;
    Ok(UserDataDto {
        uuid: upserted.user_id,
        disliked_products: upserted.dislikes,
        liked_products: upserted.likes,
        checkout_products: upserted.checkout,
        session_start: user_data.session_start,
        session_end: user_data.session_end,
        total_minutes: user_data.total_minutes,
    })
}
