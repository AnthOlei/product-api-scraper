use actix_web::{get, Result, HttpResponse};



#[get("/")]
pub async fn landing_page_route() -> Result<HttpResponse> {
    Ok(HttpResponse::build(actix_web::http::StatusCode::OK)
        .body(include_str!("index.html")))
}
