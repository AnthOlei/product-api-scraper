use crate::database::{PgPool, DatabaseError, PgPooledConnection};
use actix_web::web;

pub fn pg_pool_handler(pool: web::Data<PgPool>) -> Result<PgPooledConnection, DatabaseError> {
    Ok(pool.get().map_err(|_| DatabaseError::R2d2)?)
}