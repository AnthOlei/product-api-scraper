mod database;
mod errors;
mod models;
mod utils;

#[macro_use]
extern crate diesel;

#[macro_use]
extern crate log;

extern crate actix_web;
extern crate reqwest;
extern crate scraper;
extern crate serde;
extern crate serde_json;

use actix_web::{App, HttpServer};
use listenfd::ListenFd;

mod controllers;
use controllers::product_controller::{query_products_route, get_brands_route, get_colors_route};
use controllers::data_reporting_controller::{post_user_data_route, post_feedback_route, patch_status_route};
use controllers::scraper_controller::{scraper_route};
use controllers::landing_page_contoller::landing_page_route;
use database::{establish_connection, IntermediatePersistanceLayer};
use actix_web_static_files;

use std::collections::HashMap;

mod schema;
mod dto;

include!(concat!(env!("OUT_DIR"), "/generated.rs"));

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    let connection_pool = establish_connection();
    let ipl = IntermediatePersistanceLayer::new(&connection_pool.get().unwrap());

    let mut listenfd = ListenFd::from_env();

    let mut server = HttpServer::new(move || {
        let generated = generate();
        App::new()
            //data registration
            .data(connection_pool.clone())
            //NOTE: When using the IPL, we generally clone it and return it as quick as possible. 
            //this is done by the following syntax: *(ipl.into_inner()).clone()
            .data(ipl.clone())
            //scraper
            .service(scraper_route)
            //product
            .service(query_products_route)
            .service(get_brands_route)
            .service(get_colors_route)
            //user data
            .service(post_user_data_route)
            .service(post_feedback_route)
            .service(patch_status_route)
            //"frontend"
            .service(landing_page_route)
            .service(actix_web_static_files::ResourceFiles::new(
            "/assets", generated,
        ))
    });

    server = if let Some(l) = listenfd.take_tcp_listener(0).unwrap() {
        server.listen(l)?
    } else {
        server.bind("127.0.0.1:8080")?
    };

    server.run().await
}
