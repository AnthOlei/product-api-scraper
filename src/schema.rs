table! {
    brands (id) {
        id -> Int4,
        logo_location -> Nullable<Varchar>,
        name -> Varchar,
    }
}

table! {
    colors (id) {
        id -> Int4,
        hex -> Nullable<Varchar>,
        color -> Varchar,
    }
}

table! {
    images (id) {
        id -> Int4,
        product_id -> Int4,
        source -> Varchar,
    }
}

table! {
    kinds (id) {
        id -> Int4,
        kind -> Varchar,
    }
}

table! {
    products (id) {
        id -> Int4,
        title -> Varchar,
        price_dollar -> Int4,
        price_cents -> Int4,
        source -> Varchar,
        details -> Nullable<Varchar>,
        sale_dollar -> Nullable<Int4>,
        sale_cents -> Nullable<Int4>,
        kind_id -> Nullable<Int4>,
        brand_id -> Nullable<Int4>,
        color_id -> Nullable<Int4>,
        seller -> Varchar,
    }
}

table! {
    sessions (id) {
        id -> Int4,
        user_id -> Uuid,
        session_start -> Timestamp,
        session_end -> Timestamp,
    }
}

table! {
    user_data (user_id) {
        user_id -> Uuid,
        likes -> Array<Int4>,
        dislikes -> Array<Int4>,
        checkout -> Array<Int4>,
    }
}

table! {
    user_feedback (id) {
        id -> Int4,
        user_id -> Nullable<Uuid>,
        feedback -> Nullable<Text>,
        email -> Nullable<Varchar>,
        date_created -> Nullable<Timestamp>,
    }
}

table! {
    users (id) {
        id -> Uuid,
        total_minutes -> Int4,
        status -> Nullable<Varchar>,
    }
}

joinable!(images -> products (product_id));
joinable!(products -> brands (brand_id));
joinable!(products -> colors (color_id));
joinable!(products -> kinds (kind_id));
joinable!(sessions -> users (user_id));
joinable!(user_data -> users (user_id));
joinable!(user_feedback -> users (user_id));

allow_tables_to_appear_in_same_query!(
    brands,
    colors,
    images,
    kinds,
    products,
    sessions,
    user_data,
    user_feedback,
    users,
);
