use crate::schema::*;
use serde::{Serialize, Deserialize};
use uuid::Uuid;
use chrono::NaiveDateTime;

#[derive(Identifiable, Queryable, Associations, Insertable, Serialize, Deserialize)]
#[table_name = "products"]
pub struct Product {
    pub id: i32,
    pub title: String,
    pub price_dollar: i32,
    pub price_cents: i32,
    pub source: String,
    pub details: Option<String>,
    pub sale_dollar: Option<i32>,
    pub sale_cents: Option<i32>,
    pub kind_id: Option<i32>, //fk
    pub brand_id: Option<i32>, //fk
    pub color_id: Option<i32>, //fk
    pub seller: String,
}

impl Product {
    pub fn get_sale_price(&self) -> Option<f32> {
        let cents = (self.sale_cents? as f32) / 100.0;
        Some((self.sale_dollar? as f32) + cents)
    }

    pub fn get_price(&self) -> f32 {
        self.price_dollar as f32 + ((self.price_cents as f32) / 100.0)
    }
}

#[derive(Identifiable, Queryable, Associations, Insertable)]
#[table_name = "kinds"]
pub struct Kind {
    pub id: i32,
    pub kind: String
}

#[derive(Identifiable, Queryable, Associations, Insertable)]
#[belongs_to(Product)]
#[table_name = "images"]
pub struct Image {
    pub id: i32,
    pub product_id: i32, //fk
    pub source: String
}

#[derive(Identifiable, Queryable, Associations, Insertable)]
#[table_name = "brands"]
pub struct Brand {
    pub id: i32,
    pub logo_location: Option<String>,
    pub name: String,
}

#[derive(Identifiable, Queryable, Associations, Insertable)]
#[table_name = "colors"]
pub struct Color {
    pub id: i32,
    pub hex: Option<String>,
    pub color: String,
}

#[derive(Identifiable, Queryable, Associations, Insertable)]
#[table_name = "users"]
pub struct User {
    pub id: Uuid,
    pub total_minutes: i32,
    pub status: Option<String>,
}

#[derive(Identifiable, Queryable, Associations, Insertable, AsChangeset)]
#[primary_key(user_id)]
#[table_name = "user_data"]
pub struct UserData {
    pub user_id: Uuid,
    pub likes: Vec<i32>,
    pub dislikes: Vec<i32>,
    pub checkout: Vec<i32>
}


#[derive(Identifiable, Queryable, Associations)]
#[primary_key(user_id)]
#[table_name = "sessions"]
pub struct Session {
    pub id: i32,
    pub user_id: Uuid,
    pub session_start: NaiveDateTime,
    pub session_end: NaiveDateTime
}

#[derive(Identifiable, Queryable, Associations)]
#[table_name = "user_feedback"]
pub struct Feedback {
    pub id: i32,
    pub user_id: Option<Uuid>,
    pub feedback: Option<String>,
    pub email: Option<String>,
    pub timestamp: Option<NaiveDateTime>
}

