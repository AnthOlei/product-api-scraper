use super::ProductDto;
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize)]
pub struct ProductListDto {
    pub products: Vec<ProductDto>,
}