use serde::{Serialize, Deserialize};
use crate::models::Image;

#[derive(Serialize, Deserialize)]
pub struct ImageDto {
    url: String,
}

impl ImageDto {
    pub fn from_entity(image: Image) -> ImageDto {
        ImageDto { url: image.source }
    }
}
