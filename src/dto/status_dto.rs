use serde::{Serialize, Deserialize};
use uuid::Uuid;

#[derive(Serialize, Deserialize)]
pub struct StatusDto {
    pub user_id: Uuid,
    pub status: String
}