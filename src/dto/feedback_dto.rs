use serde::{Serialize, Deserialize};
use crate::models::Feedback;
use uuid::Uuid;

#[derive(Serialize, Deserialize)]
pub struct FeedbackDto {
    pub user_id: Option<Uuid>,
    pub feedback: Option<String>,
    pub email: Option<String>
}

impl FeedbackDto {
    pub fn from_entity(feedback: Feedback) -> FeedbackDto {
        FeedbackDto {
            user_id: feedback.user_id,
            feedback: feedback.feedback,
            email: feedback.email,
        }
    }
}