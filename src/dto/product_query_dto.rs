use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug)]
pub struct ProductQueryDto {
    //all price bounds are dollars. ignore cents, level of percision not needed.
    pub price_upper_bound: Option<i32>,
    pub price_lower_bound: Option<i32>,

    //names of fk's
    pub kind: Option<String>,
    pub brand: Option<String>,
    pub color: Option<String>,

    //amount of products fetched
    pub count: i32,

    //products FE has already seem
    pub seen: Vec<i32>,
}