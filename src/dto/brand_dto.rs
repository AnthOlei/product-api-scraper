use crate::models::Brand;
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize)]
pub struct BrandDto {
    name: String,
    logo_location: Option<String>,
}

impl BrandDto {
    pub fn from_model(brand: Brand) -> BrandDto {
        BrandDto {
            name: brand.name,
            logo_location: brand.logo_location,
        }
    }
}