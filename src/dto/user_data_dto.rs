use serde::{Deserialize, Serialize};
use uuid::Uuid;
use chrono::{DateTime, Utc};

#[derive(Deserialize, Serialize, Debug)]
pub struct UserDataDto {
    pub uuid: Uuid, //convertable into uuid type
    pub session_start: Option<DateTime::<Utc>>, //comes in as UTC datetime
    pub session_end: Option<DateTime::<Utc>>,
    pub disliked_products: Vec<i32>,
    pub liked_products: Vec<i32>,
    pub checkout_products: Vec<i32>,
    pub total_minutes: i32,
}