use crate::models::{Product, Color, Kind, Brand, Image};
use crate::database::DatabaseError;
use crate::utils::price_from_ints;
use super::{ImageDto, BrandDto};
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize)]
pub struct ProductDto {
    pub id: i32,
    pub title: String,
    pub source: String,
    pub details: Option<String>,
    pub price: f32,
    pub sale: Option<f32>,
    pub images: Vec<ImageDto>,
    pub kind: Option<String>, //fk
    pub brand: Option<BrandDto>, //fk
    pub color: Option<String>, //fk
    pub seller: String,
}

impl ProductDto {
    pub fn full_from_entites(product: Product, brand: Option<Brand>, color: Option<Color>, kind: Option<Kind>, images: Vec<Image>) -> Result<ProductDto, DatabaseError> {
        Ok(ProductDto {
            id: product.id,
            title: product.title.clone(),
            price: price_from_ints(product.price_dollar, product.price_cents),
            sale: product.sale_dollar.map(|sale_dollar|{
                price_from_ints(sale_dollar, product.sale_cents.unwrap_or_default()) //default so incase some malformed data, we don't panic
            }),
            brand: brand.map(BrandDto::from_model),
            color: color.map(|color|{color.color}),
            details: product.details,
            images: images.into_iter().map(ImageDto::from_entity).collect(),
            kind: kind.map(|kind|{kind.kind}),
            source: product.source,
            seller: product.seller,
        })
    }
}