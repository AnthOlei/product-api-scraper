use crate::controllers::scraper_controller::errors::ScraperError as scraperError;
use crate::database::DatabaseError as databaseError;
use actix_http::ResponseBuilder;
use actix_web::{error, http::header, http::StatusCode, HttpResponse};
use failure::Fail;
use serde::Serialize;
use std::fmt;

// this is a USER FACING error, but we aim to be as descriptive as possible
// because this is an API.

#[derive(Debug, Fail)]
pub enum ApiError {
    ScraperError(scraperError),
    DatabaseError(databaseError),
}

impl fmt::Display for ApiError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &*self {
            ApiError::ScraperError(e) => e.fmt(f),
            ApiError::DatabaseError(e) => e.fmt(f),
        }
    }
}

impl std::convert::From<scraperError> for ApiError {
    fn from(error: scraperError) -> Self {
        ApiError::ScraperError(error)
    }
}

impl std::convert::From<databaseError> for ApiError {
    fn from(error: databaseError) -> Self {
        ApiError::DatabaseError(error)
    }
}

impl error::ResponseError for ApiError {
    fn error_response(&self) -> HttpResponse {
        ResponseBuilder::new(self.status_code())
            .set_header(header::CONTENT_TYPE, "application/json; charset=utf-8")
            .body(generate_error(self.to_string()))
    }

    fn status_code(&self) -> StatusCode {
        match self {
            ApiError::ScraperError(_error) => StatusCode::INTERNAL_SERVER_ERROR,
            ApiError::DatabaseError(_error) => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}


pub fn generate_error(error: String) -> String {
    #[derive(Serialize)]
    struct ErrorJson {
        error: String,
    }

    serde_json::to_string(&ErrorJson { error }).unwrap()
}
