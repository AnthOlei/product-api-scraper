use super::errors::DatabaseError;
use super::insert::{BrandInsertable, ColorInsertable};
use super::queries::{get_brands, get_colors, get_kinds};
use crate::diesel::RunQueryDsl;
use crate::models::{Brand, Color};
use crate::database::PgPooledConnection;
use std::collections::HashMap;
use std::sync::{RwLock, Arc};

//TODO Refactor this. make this persist throught session
//required to locally store the database entries
//so we don't have to requery all the (for example) colors
//every time we add a new product.

#[derive(Clone)]
pub struct IntermediatePersistanceLayer {
    //refrence from name to id.
    brands: Arc<RwLock<HashMap<String, i32>>>,
    colors: Arc<RwLock<HashMap<String, i32>>>,
    kinds: Arc<RwLock<HashMap<String, i32>>>,
}

impl IntermediatePersistanceLayer {
    //constructs IPL from queries. using a database connection.
    pub fn new(conn: &PgPooledConnection) -> IntermediatePersistanceLayer {
        IntermediatePersistanceLayer {
            brands: Arc::new(RwLock::new(get_brands(conn))),
            colors: Arc::new(RwLock::new(get_colors(conn))),
            kinds: Arc::new(RwLock::new(get_kinds(conn))),
        }
    }
}

//BRAND insertions
//only pub fn is get_or_insert_brand
impl IntermediatePersistanceLayer {
    //gets the FK of a brand name.
    pub fn get_or_insert_brand(&self, value: String, connection: &PgPooledConnection) -> Result<i32, DatabaseError> {
        let read_lock = self.brands.read().unwrap();
        match read_lock.get(&value).to_owned() {
            None => {
                drop(read_lock);
                self.insert_brand(value, connection)
            },
            Some(v) => Ok(v.to_owned()),
        }
    }

    //local use only. helper for if we KNOW (to our best knowledge) that it is not
    //already in the database.
    fn insert_brand(&self, value: String, connection: &PgPooledConnection) -> Result<i32, DatabaseError> {
        use crate::schema::brands;

        Ok(diesel::insert_into(brands::table)
            .values(BrandInsertable {
                name: value.clone(), //need clone so we can also use it in the closure
                logo_location: None,
            })
            .get_result::<Brand>(connection)
            .and_then(|query_result| {
                self.brands.write().unwrap().insert(value.clone(), query_result.id);
                //safe to unwrap here, we know it's in the databse
                Ok(query_result.id)
            })?)
    }
}

//BRAND insertions
//only pub fn is get_or_insert_brand
impl IntermediatePersistanceLayer {
    //gets the FK of a brand name.
    pub fn get_or_insert_color(&self, value: String, connection: &PgPooledConnection) -> Result<i32, DatabaseError> {
        let read_lock = self.colors.read().unwrap();
        match read_lock.get(&value).to_owned() {
            None => {
                drop(read_lock);
                self.insert_color(value, connection)
            },
            Some(v) => Ok(v.to_owned()),
        }
    }

    pub fn get_color(&self, value: String) -> Option<i32> {
        self.colors.read().unwrap().get(&value).map(|v| v.to_owned())
    }

    //local use only. helper for if we KNOW (to our best knowledge) that it is not
    //already in the database.
    fn insert_color(&self, value: String, connection: &PgPooledConnection) -> Result<i32, DatabaseError> {
        use crate::schema::colors;

        Ok(diesel::insert_into(colors::table)
            .values(ColorInsertable {
                color: value.clone(), //need clone so we can also use it in the closure
                hex: None,
            })
            .get_result::<Color>(connection)
            .and_then(|query_result| {
                self.colors.write().unwrap().insert(value.clone(), query_result.id);
                Ok(query_result.id)
            })?)
    }
}

impl IntermediatePersistanceLayer {
    pub fn get_brand_id(&self, brand: String) -> Option<i32> {
        self.brands.read().unwrap().get(&brand).map(|v| v.to_owned())
    }

    pub fn get_color_id(&self, color: String) -> Option<i32> {
        self.colors.read().unwrap().get(&color).map(|v| v.to_owned())
    }

    pub fn get_kind_id(&self, kind: String) -> Option<i32> {
        self.kinds.read().unwrap().get(&kind).map(|v| v.to_owned())
    }
}

impl IntermediatePersistanceLayer {
    //getter
    pub fn get_kinds(&self) -> HashMap<String, i32> {
        self.kinds.read().unwrap().clone()
    }

    pub fn get_colors(&self) -> Vec<String> {
         self.colors.read().unwrap().keys().map(|k| k.to_owned()).collect()
    }

    pub fn get_brands(&self) -> Vec<String> {
        self.brands.read().unwrap().keys().map(|k| k.to_owned()).collect()
    }
}
