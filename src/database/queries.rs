use crate::models::{Brand, Color, Kind};
use std::collections::HashMap;
use crate::diesel::prelude::*;
use diesel::pg::PgConnection;

//TODO anyway to get dynamically?? similar implimentation for each, with only the table name, and property changed...

//hashmap with kind as key and ID as value. this is necessary because we don't want to keep
//requering to get the ID and check if a certain brand is in a table, so instead we create a local
//refrence, for both brands and colors and kind. 
pub fn get_brands(connection: &PgConnection) -> HashMap<String, i32> {
    use crate::schema::brands::dsl::*;
    let mut brand_map = HashMap::<String, i32>::new();
    let results = brands
        .load::<Brand>(connection)
        .expect("error loading brands");

    for brand_model in results {
        brand_map.insert(brand_model.name, brand_model.id);
    }

    brand_map
}

pub fn get_colors(connection: &PgConnection) -> HashMap<String, i32> {
    use crate::schema::colors::dsl::*;
    let mut color_map = HashMap::<String, i32>::new();
    let results = colors
        .load::<Color>(connection)
        .expect("error loading colors");

    for color_model in results {
        color_map.insert(color_model.color, color_model.id);
    }
    color_map
}

pub fn get_kinds(connection: &PgConnection) -> HashMap<String, i32> {
    use crate::schema::kinds::dsl::*;
    let mut kind_map = HashMap::<String, i32>::new();
    let results = kinds
        .load::<Kind>(connection)
        .expect("error loading colors");

    for kind_model in results {
        kind_map.insert(kind_model.kind, kind_model.id);
    }
    kind_map
}