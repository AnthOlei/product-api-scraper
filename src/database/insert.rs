use crate::schema::*;
use chrono::NaiveDateTime;
use uuid::Uuid;

#[derive(Insertable)] //TODO figure out what derserialize is here... Serede?
#[table_name = "products"]
pub struct ProductInsertable {
    pub title: String,
    pub price_dollar: i32,
    pub price_cents: i32,
    pub details: Option<String>,
    pub source: String,
    pub sale_dollar: Option<i32>,
    pub sale_cents: Option<i32>,
    pub kind_id: Option<i32>,  //fk
    pub brand_id: Option<i32>, //fk
    pub color_id: Option<i32>, //fk
    pub seller: String,
}

#[derive(Insertable)]
#[table_name = "brands"]
pub struct BrandInsertable {
    pub logo_location: Option<String>,
    pub name:  String,
} 

#[derive(Insertable)]
#[table_name = "colors"]
pub struct ColorInsertable {
    pub color: String,
    pub hex: Option<String>,
} 

#[derive(Insertable)]
#[table_name = "images"]
pub struct ImageInsertable {
    pub product_id: i32,
    pub source: String,
} 

#[derive(Insertable)]
#[table_name = "sessions"]
pub struct SessionInsertable {
    pub user_id: Uuid,
    pub session_start: NaiveDateTime,
    pub session_end: NaiveDateTime,
} 

#[derive(Insertable)]
#[table_name = "user_feedback"]
pub struct FeedbackInsertable {
    pub user_id: Option<Uuid>,
    pub feedback: Option<String>,
    pub email: Option<String>,
    pub date_created: Option<NaiveDateTime>,
}