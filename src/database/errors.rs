
use failure::Fail;
use std::fmt;
use diesel::result::Error as dieselError;

#[derive(Debug, Fail)]
pub enum DatabaseError {
    R2d2,
    InvalidStatusValue,
    DieselError(dieselError),
}

impl fmt::Display for DatabaseError {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &*self {
            DatabaseError::DieselError(ref e) => e.fmt(f),
            R2d2 => write!(f, "Couldn't establish pooled connection"),
            InvalidStatusValue => write!(f, "Invalid status value: Status must be assigned or unassigned"),
        }
    }
}

impl std::convert::From<dieselError> for DatabaseError {
    fn from(error: dieselError) -> Self {
        DatabaseError::DieselError(error)
    }
}
