CREATE TABLE users (
  id UUID PRIMARY KEY
);

CREATE TABLE sessions (
  id SERIAL PRIMARY KEY,
  user_id UUID references users(id) NOT NULL,
  session_start TIMESTAMP NOT NULL,
  session_end TIMESTAMP NOT NULL
);

CREATE TABLE user_data (
    user_id UUID references users(id) PRIMARY KEY,
    likes INTEGER [] NOT NULL,
    dislikes INTEGER [] NOT NULL,
    checkout INTEGER [] NOT NULL
);