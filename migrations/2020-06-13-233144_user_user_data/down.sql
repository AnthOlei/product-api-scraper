-- This file should undo anything in `up.sql`
DROP TABLE user_data cascade;
DROP TABLE sessions cascade;
DROP TABLE users cascade;