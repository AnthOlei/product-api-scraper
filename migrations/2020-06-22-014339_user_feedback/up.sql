-- Your SQL goes here
CREATE TABLE user_feedback (
    id SERIAL PRIMARY KEY,
    user_id UUID references users(id),
    feedback TEXT,
    email VARCHAR,
    date_created TIMESTAMP DEFAULT Now()
);