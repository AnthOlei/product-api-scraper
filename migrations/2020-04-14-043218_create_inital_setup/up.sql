CREATE TABLE brands (
  id SERIAL PRIMARY KEY,
  logo_location VARCHAR,
  name VARCHAR NOT NULL --name is not reserved, it just might be so VSCode makes it blue.
);


CREATE TABLE colors (
  id SERIAL PRIMARY KEY,
  hex VARCHAR(6),
  color VARCHAR NOT NULL
);

CREATE TABLE kinds (
  id SERIAL PRIMARY KEY,
  kind VARCHAR NOT NULL
);

CREATE TABLE products (
  id SERIAL PRIMARY KEY,
  title VARCHAR NOT NULL,
  price_dollar INTEGER NOT NULL,
  price_cents INTEGER NOT NULL,
  source VARCHAR NOT NULL, 
  details VARCHAR,
  sale_dollar INTEGER,
  sale_cents INTEGER,
  kind_id INTEGER references kinds(id),
  brand_id INTEGER references brands(id),
  color_id INTEGER references colors(id)
);


CREATE TABLE images (
    id SERIAL PRIMARY KEY,
    product_id INTEGER  NOT NULL references products(id),
    source VARCHAR NOT NULL
);

