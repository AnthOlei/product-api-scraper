-- This file should undo anything in `up.sql'
DROP TABLE products cascade;
DROP TABLE kinds cascade;
DROP TABLE images cascade;
DROP TABLE brands cascade;
DROP TABLE colors cascade;